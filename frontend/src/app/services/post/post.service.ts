import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppSettings} from '../../settings/app.settings';
import {Post} from '../../models/post';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }

  FindAll() {
    return this.http.get<Post[]>(AppSettings.POST_URL + '/all');
  }

  FindBySujets(idSujet: number) {
    return this.http.get<Post[]>(AppSettings.POST_URL + '/' + idSujet);
  }
}
