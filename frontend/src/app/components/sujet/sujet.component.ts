import { Component, OnInit } from '@angular/core';
import {Sujet} from '../../models/sujet';
import {SujetService} from '../../services/sujet/sujet.service';
import {PostService} from '../../services/post/post.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-sujet',
  templateUrl: './sujet.component.html',
  styleUrls: ['./sujet.component.css'],
  providers: [SujetService]
})
export class SujetComponent implements OnInit {

  sujet: Sujet;

  constructor(private sujetService: SujetService, private postService: PostService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe(routeParams => {
      this.loadUserDetail(routeParams.id);
    });
  }

  loadUserDetail(id: number) {
    this.findOne(id);
  }

  findOne(id: number) {
    this.sujetService.findOneSujet(id)
      .pipe()
      .subscribe(data => {
        console.log(data);
        this.sujet = data;
      }, error => {
        console.log(error);
        const link = ['/accueil'];
        this.router.navigate(link);
      });
  }
}
