package com.lod.forum.controllers;

import com.lod.forum.entities.Categorie;
import com.lod.forum.repositories.ICategorie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/categories")
public class CategorieController {

    @Autowired
    private ICategorie categorieRepository;
    private int[] categorieArray= new int[]{1, 2, 3, 4};

    @GetMapping("/{idCategorie}")
    public ResponseEntity findById(@PathVariable(name = "idCategorie") Long idCategorie) {
        if (idCategorie == null) {
            return ResponseEntity.badRequest().body("Cannot retrieve categorie with null ID");
        }
        List<Categorie> tempCategorie = categorieRepository.findAll();
        if(idCategorie < 1 || idCategorie > tempCategorie.size()){
            return ResponseEntity.badRequest().body("Aucune catégorie existante pour cet ID");
        }
        Categorie categorie = categorieRepository.getOne(idCategorie);
        if (categorie == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(categorie);
    }

    @GetMapping("/accueil")
    public ResponseEntity findCategorieAccueil() {
        List<Categorie> allCategorie = categorieRepository.findAll();
        if (allCategorie == null) {
            return ResponseEntity.notFound().build();
        }
        List<Categorie> allAccueil = new ArrayList<Categorie>();
        for (Categorie categorie:
             allCategorie) {
            for (int id:
            categorieArray) {
                if (categorie.getId() == id) {
                    allAccueil.add(categorie);
                }
            }
        }
        if (allAccueil == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(allAccueil);
    }

    @GetMapping("/sub/{association}")
    public ResponseEntity findListByAssociation(@PathVariable(name = "association") Long association) {
        if (association == null){
            return ResponseEntity.badRequest().body("Cannot retrieve association number");
        }
        List<Categorie> listAssociation = this.categorieRepository.findByAssociation(association);
        if (listAssociation != null && listAssociation.size() > 0 && listAssociation.get(0).getId() == association){
            listAssociation.remove(0);
        }
        if (listAssociation == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(listAssociation);
    }

    @GetMapping("/all")
    public ResponseEntity findAllCategories() {
        List<Categorie> categorieList = categorieRepository.findAll();
        if (categorieList == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(categorieList);
    }
}
