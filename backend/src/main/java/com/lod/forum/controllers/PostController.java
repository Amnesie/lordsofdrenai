package com.lod.forum.controllers;

import com.lod.forum.entities.Post;
import com.lod.forum.entities.Sujet;
import com.lod.forum.repositories.ICategorie;
import com.lod.forum.repositories.IPost;
import com.lod.forum.repositories.ISujet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/posts")
public class PostController {

    @Autowired
    private IPost postRepository;

    @GetMapping("/all")
    public ResponseEntity findAllPosts() {
        List<Post> postList = postRepository.findAll();
        if (postList == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(postList);
    }

    @GetMapping("/{idSujet}")
    public ResponseEntity findBySujets(@PathVariable(name = "idSujet") Long idSujet) {
        List<Post> postList = postRepository.findByIdSujet(idSujet);
        if (postList == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(postList);
    }
}
