package com.lod.forum.controllers;

import com.lod.forum.entities.Categorie;
import com.lod.forum.entities.Post;
import com.lod.forum.entities.Sujet;
import com.lod.forum.repositories.ICategorie;
import com.lod.forum.repositories.ISujet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/sujets")
public class SujetController {

    @Autowired
    private ISujet sujetRepository;

    @GetMapping("/all/{idCategorie}")
    public ResponseEntity findSujetsByCategorie(@PathVariable(name = "idCategorie") Long idCategorie) {
        if (idCategorie == null) {
            return ResponseEntity.badRequest().body("Cannot retrieve categorie with null ID");
        }
        List<Sujet> sujets = sujetRepository.findByIdCategorie(idCategorie);
        if (sujets == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(sujets);
    }

    @GetMapping("/{idSujet}")
    public ResponseEntity findBySujets(@PathVariable(name = "idSujet") Long idSujet) {
        if (idSujet == null) {
            return ResponseEntity.badRequest().body("Cannot retrieve sujet with null ID");
        }
        List<Sujet> tempSujet = sujetRepository.findAll();
        if(idSujet < 1 || idSujet > tempSujet.size()){
            return ResponseEntity.badRequest().body("Aucun sujet existant pour cet ID");
        }
        Sujet sujet = sujetRepository.getOne(idSujet);
        if (sujet == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(sujet);
    }

    @GetMapping("/all")
    public ResponseEntity findAllSujets() {
        List<Sujet> sujetList = sujetRepository.findAll();
        if (sujetList == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(sujetList);
    }
}
