package com.lod.forum.repositories;

import com.lod.forum.entities.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUtilisateur extends JpaRepository<Utilisateur, Long> {

    Utilisateur findByPseudoAndMdp(String pseudo, String mdp);
}
